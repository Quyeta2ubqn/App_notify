﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Threading;
using Caliburn.Micro;
using Notifications.Wpf.Caliburn.Micro.Sample.ViewModels;
using Npgsql;

namespace Notifications.Wpf.Caliburn.Micro.Sample
{
    class Bootstrapper : BootstrapperBase
    {
        SimpleContainer _container = new SimpleContainer();
        public List<string> kotoba_JP { get; set; }
        public List<string> kotoba_EN { get; set; }

        public List<string> imi { get; set; }

        public List<NotificationContent> notificationContents { get; set; }
        public List<string> msg
            { get; set; }

        public Bootstrapper( )
        {

            msg = new List<string>();
            notificationContents = new List<NotificationContent>(); 
            Initialize();
            Connection(msg, notificationContents);
        }




        public static void Connection(List<string> msg, List<NotificationContent> notificationContents) {
            using (NpgsqlConnection conn = GetConnection()) {
                conn.Open();
                if (conn.State == System.Data.ConnectionState.Open) {
                    // Get data
                    // Define a query
                    NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM Tu_vung", conn);
                    NpgsqlDataReader dr = command.ExecuteReader();
                    // Output rows
                    while (dr.Read())
                    {
                        //msg.Add(dr["kotoba_JP"].ToString() + "\r\n" + dr["kotoba_EN"].ToString() + "\r\n" + dr["imi"].ToString());
                        NotificationContent tmp = new NotificationContent();
                        tmp.Title = dr["kotoba_JP"].ToString();
                        tmp.Message = dr["kotoba_EN"].ToString() + "\r\n" + dr["imi"].ToString();
                        tmp.Type = NotificationType.Information;
                        notificationContents.Add(tmp);
                    }    
                }
            }
        }
        private static NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(@"Server=localhost;Port=5433;User Id=postgres;Password=postgres;Database=Web_JP;");
        }

        protected override void Configure()
        {
            base.Configure();

            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<INotificationManager, NotificationManager>();
            _container.Singleton<ShellViewModel>();
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
        
            Random random = new Random();
            base.OnStartup(sender, e);
            DisplayRootViewFor<ShellViewModel>();
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            

            var timer = new Timer { Interval = 7*1000};
            int index = 0;
            timer.Elapsed += (o, args) =>  IoC.Get<INotificationManager>()
                    .Show(notificationContents[random.Next(notificationContents.Count)],
                                                    expirationTime: TimeSpan.FromSeconds(7));
                timer.Start();


  
        }


    }
}
